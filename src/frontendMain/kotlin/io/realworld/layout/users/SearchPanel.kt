@file:UseContextualSerialization(Date::class)


package io.realworld.layout.users

import io.kvision.core.onEvent
import io.kvision.form.FormPanel
import io.kvision.form.FormType
import io.kvision.form.formPanel
import io.kvision.form.select.Select
import io.kvision.form.select.SimpleSelect
import io.kvision.form.text.Text
import io.kvision.html.Button
import io.kvision.html.ButtonStyle
import io.kvision.i18n.I18n.tr
import io.kvision.panel.SimplePanel
import io.kvision.state.bind
import io.kvision.utils.px
import io.realworld.ConduitManager
import io.realworld.ConduitState
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseContextualSerialization
import kotlin.js.Date

@Serializable
data class MySearch(
    val keyword: String? = null,
    val category: String? = null,
)
@Serializable
enum class Category {
    ONE, TWO, THREE
}

class SearchPanel(private val state: ConduitState) : SimplePanel(classes = setOf()) {
    private val searchPanel: FormPanel<MySearch>

    init {
        this.marginTop = 10.px
        this.addCssClass("d-flex")
        this.addCssClass("justify-content-center")
        this.addCssClass("main-search-div")

        searchPanel = formPanel<MySearch>(
            className = "form-inline main-search-panel d-flex justify-content-center",
            type = FormType.INLINE
        ).bind(ConduitManager.conduitStore) { state1 ->

            add(
                MySearch::category,
                SimpleSelect(
                    value = state1.currentSearch.category,
                    options = listOf(Pair("*", "All categories")) + (Category.values().map { it.name to tr(it.name) }),
                ) {
                    //selectedIndex = 0
                    addCssClass("col-xs-12")
                }
            )
            add(
                MySearch::category,
                Select(
                    value = state1.currentSearch.category,
                    options = listOf(Pair("*", "All categories")) + (Category.values().map { it.name to tr(it.name) }),
                ) {
                    //selectedIndex = 0
                    addCssClass("col-xs-12")
                }
            )
            add(MySearch::keyword, Text(
                value = state1.currentSearch.keyword
            ) {
                placeholder = tr("Keywords...")
                input.addCssClass("search-text").addCssClass("col-xs-12")
            })
            add(
                Button("", "fas fa-search", ButtonStyle.OUTLINEPRIMARY, classes = setOf("col-xs-2"))
                    .onClick {
                        this@SearchPanel.search(state1)
                    }
            )
        }

        searchPanel.onEvent {
            submit = { ev ->
                ev.preventDefault()
                this@SearchPanel.search(state)
            }
        }
    }

    private fun search(state: ConduitState) {
        GlobalScope.launch {
            if (searchPanel.validate()) {
                ConduitManager.filter(searchPanel.getData())
            } else {
                console.log("search form invalid")
            }
        }
    }
}